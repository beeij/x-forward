export enum EventEnum {
    CONFIG_CREATE = 'config.create',
    CONFIG_UPDATE = 'config.update',
    CONFIG_DELETE = 'config.delete',
    CLIENT_PORT_ADD = 'client.port.add',
    CLIENT_PORT_REMOVE = 'client.port.remove'
}
