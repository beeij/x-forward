export enum ShellEnum {
    CAT = 'cat',
    TOUCH = 'touch',
    LS = 'ls',
    WHICH = 'which',
    ECHO = 'echo',
    GREP = 'grep',
    CP = 'cp',
    BASH = 'bash',
    MV = 'mv',
    SERVICE = 'service',
    SYSTEMCTL = 'systemctl',
    UNAME = 'uname',
    LSB_RELEASE = 'lsb_release'
}

export enum ServiceEnum {
    STATUS = 'status',
    START = 'start',
    RESTART = 'restart',
    STOP = 'stop'
}
