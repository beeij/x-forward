enum CacheEnum {
    PortAndUserRelation = 'PORT_AND_USER_RELATION',
    ClientId = 'CLIENT_ID'
}

export default CacheEnum
