export enum TimeUnitEnum {
    Second = 's',
    Minute = 'm',
    Hours = 'h',
    Day = 'd'
}

export enum SpeedUnitEnum {
    Byte = 'k'
}
