export enum CommonEnum {
    PlaceHolder = '-',
    CreateTime = '创建时间',
    UpdateTime = '更新时间',
    DeleteTime = '删除时间'
}

export enum IsOrNotEnum {
    False,
    True
}
